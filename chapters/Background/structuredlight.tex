\section{Camera Geometry}
Camera geometry is the theory behind image formation and how it relates to the 3D geometry of a given scene. This is understood as both the geometry of the scene itself and the camera's spatial properties. Camera geometry is the foundation of the vast majority of 3D estimation techniques such as stereo-vision, structured light, structure from motion and non-rigid structure from motion.

In this thesis, camera geometry is described in terms of projective models. That is, a model that can tell us a given 3D point's projection on the image plane. We will primarily be dealing with two camera models; orthographic and pinhole. The orthographic camera is arguably the simplest camera which finds usage in structure from motion and non-rigid structure from motion. Here, a 3D point $\mathbf{x}$ is projected along a line orthogonal to image plane $I$. Mathematically it is modeled as;
\begin{align}
\mathbf{u} = \begin{bmatrix}
 1 & 0 & 0 & 0\\
 0 & 1 & 0 & 0\\
 0 & 0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
  s\mathbf{R}  & \mathbf{t}\\
  \mathbf{0}  & 1
\end{bmatrix}\mathbf{x},
\end{align}
where,
\begin{align*}
  \mathbf{R} &= \text{rotation matrix},\\
  \mathbf{t} &= \text{translation vector},\\
  s          &= \text{scale},\\
  \mathbf{u} &= \text{$\mathbf{x}$'s projection in homogenous coordinates}.
\end{align*}
As can be seen, an orthographic camera simply discards the z-coordinate during projection. This camera model is illustrated on the left side of Figure~\ref{fig:camera_models}. While the orthographic camera provides a decent model of real world cameras for small volumes, it does not work well with large object or distances. The reason is that it does not model perspective foreshortening.

\begin{figure}
  \centering
  \begin{minipage}[t]{0.45\textwidth}
    \centering
    \includegraphics[width=\textwidth]{graphics/background/orthopgrahic_camera}
  \end{minipage}
  \begin{minipage}[t]{0.4\textwidth}
    \centering
    \includegraphics[width=\textwidth]{graphics/background/pinhole_camera}
  \end{minipage}
  \caption{Illustration of orthographic camera to the left and pinhole to the right.}
  \label{fig:camera_models}
\end{figure}

The pinhole camera model is a more accurate representation of real-world cameras. Here all projection lines must pass through a single point $\mathbf{f}$ called the focal point. Mathematically it is described as;
\begin{align}
    \mathbf{u}  &= \underbrace{
                    \begin{bmatrix}
                      f_x & 0 & c_x\\
                      0 & f_y & c_y\\
                      0 & 0 & 1
                    \end{bmatrix}
                  }_{\mathbf{A}}
                  \underbrace{
                    \begin{bmatrix}
                      &           \\
                      \mathbf{R}  & \mathbf{t}\\
                      &
                    \end{bmatrix}
                  }_{\mathbf{E}}
                  \mathbf{x},\label{eq:pinhole}
\end{align}
where,
\begin{align*}
  \mathbf{R} &= \text{rotation matrix},\\
  \mathbf{t} &= \text{translation vector},\\
  \mathbf{u} &= \text{$\mathbf{x}$'s projection in homogenous coordinates},\\
  f_x, f_y   &= \text{focal length},\\
  c_x, c_y   &= \text{principal point},\\
  \mathbf{A} &= \text{intrinsic matrix},\\
  \mathbf{E} &= \text{extrinsic matrix}.
\end{align*}
Unlike the orthographic camera, the pinhole camera models perspective foreshortening. For this reason it is also sometimes referred to as a perspective camera. It can even be extended to model lens distortion~\cite{duane1971close}. Overall it is a quite good representation of most real-world cameras despite leaving out concepts such as depth of field and focus. Parameters for the model in \eqref{eq:pinhole} for a given camera can be efficiently estimated calibrated using e.g. the method of~\cite{zhang2000flexible}.


\begin{figure}
  \centering
  \includegraphics[width=0.5\textwidth]{graphics/background/triangulate}
  \caption{Geometry of a pair of pinhole cameras. $\mathbf{F}_1$ and $\mathbf{F}_2$ are the focal points, $\mathbf{u}_1$ and $\mathbf{u}_2$ are perspective projections of $\mathbf{X}$, and $P_1$ and $P_2$ are the projective lines}
  \label{fig:tri}
  \includegraphics[width=0.5\textwidth]{graphics/background/epipolar_geometry}
  \caption{Epipolar geometry of the correspondence problem. The gray is the plane spanned by focal points $\mathbf{F}_1$ and $\mathbf{F}_2$ as well as $\mathbf{u}_2$ which is $\mathbf{X}$'s projection along $P_1$. This planes intersection with the image plane of $\mathbf{F}_2$ is the epipolar line $e_2$ on which $\mathbf{u}_2$ can be found}
  \label{fig:epipolar_geo}
\end{figure}

\subsection{Epipolar Geometry}
Neither of the above camera models are invertible, that is you cannot deduce a point's 3D position from it's projection onto the image. However 3D estimation is possible using two or more observations of the same point. Indeed, much of a human's sense of depth is a result of us having two eyes and can thus make two simultaneous observations. To understand how, we need to take a look at the geometry of a point and two pinhole cameras. Consider the notation of of Figure~\ref{fig:tri}.
%Let us say that we want to estimate $\mathbf{X}$ and that we the positions of each camera's focal point $\mathbf{F}_1$ and $\mathbf{F}_2$ along $\mathbf{u}_1$ and $\mathbf{u}_2$ which are the projections of $\mathbf{X}$ onto each image.
Suppose we would like to estimate $\mathbf{X}$ and we know of $\mathbf{X}$'s projection onto both camera images; $\mathbf{u}_1$ and $\mathbf{u}_2$. Also suppose that we know the positions of each camera $\mathbf{F}_1$ and $\mathbf{F}_2$ along the corresponding intrinsics.
With this information we can deduce the projective lines $P_1$ and $P_2$. We known that $\mathbf{X}$ must lie on both $P_1$ and $P_2$, therefore $\mathbf{X}$ must lie on the intersection between $P_1$ and $P_2$. This process of finding $\mathbf{X}$ using projective lines is known as \textbf{triangulation}.



%For real-world data the exact position of $\mathbf{u}_1$ and $\mathbf{u}_2$ is often contaminated with noise, and therefore it is common for $P_1$ and $P_2$ to get close, but not intersect. So for practical applications in e.g. a structured light scanner $\mathbf{X}$ is deduced in a least-squares distance sense.\fxnote{refer to multiview book thing}\fxnote{this is probably unnecessay due to epipolar geomtry}


In practice, the positions of $\mathbf{F}_1$ and $\mathbf{F}_2$ can be determined beforehand in a calibration step~\cite{itseez2015opencv}, but $\mathbf{u}_1$ and $\mathbf{u}_2$ must be deduce using the available image data. This is typically formulated as a pair assignment problem, meaning given some $\mathbf{u}_1$ we would like to deduce it's correspondence $\mathbf{u}_2$ in the other image. This is problem known as the correspondence problem. Fortunately, the camera geometry itself can be used to constraint the search space.

Let us say that we know $\mathbf{F}_1$, $\mathbf{F}_2$ and $\mathbf{u}_1$ and we would like to find $\mathbf{u}_2$. Consider the plane spanned by $\mathbf{F}_1$, $\mathbf{F}_2$ and $\mathbf{u}_1$, we known that $\mathbf{X}$, $P_2$ and $\mathbf{u}_2$ must also lie in this plane. This plane is illustrated as the gray triangle shape in Figure~\ref{fig:epipolar_geo}.
The intersection between this plane and the image plane of $\mathbf{F}_2$ gives us a line on which $\mathbf{u}_2$ lies. This is referred to as an \textbf{epipolar} line and is shown in Figure~\ref{fig:epipolar_geo} as $e_2$. Thus epipolar geometry is used to constrain the search space for the correspondence problem with known camera geometry. For specifics, please consult~\cite{Hartley2004}.

Even with the constraints of epipolar geometry, the correspondence problem can be quite hard to solve accurately for natural images due to weak or repeating texture. Structured light is an effective way of overcoming these limitation and often provides much more accurate 3D data than passive stereo.


\section{Structured Light}
\label{sec:bg:stl}
Structured light is an active depth measurement technique, which eases the correspondence problem by projecting artificial texture onto the measured scene. The simplest example is merely projecting random noise, but typically the projected patterns are structured such that they directly encode positional information. Structured light can be implemented in many ways, but is typically achieved with a light projector and cameras, as was done in this thesis. The many structured light methods that have been developed over the years can be roughly grouped according to two factors;

\begin{description}
  \item[Camera] \hfill \\Single or Multi. Meaning whether a single or multiple cameras are used. For single camera the correspondence search is performed between the camera and the projector. For multi the correspondence search is performed between the cameras.
  \item[Pattern] \hfill \\One-shot or multiplex. Meaning whether one or multiple patterns are projected onto the scene. The former is often used for real-time applications and the latter is often used for precision measurements.
\end{description}

%In this thesis, we had no need for real-time performance, so multiplexing structured light was solely used. Therefore it will also be the focus of the following text.

\begin{table}
  \centering
  \caption{Structured light taxonomy with cited examples for each entry.}
  \label{tab:stl_taxonomy}
  \begin{tabular}{l r r}\toprule
                  & \textbf{Oneshot} & \textbf{Multiplex}\\
    \textbf{Single-Camera} & Kinect V1  & SLStudio~\cite{wilm2014slstudio}\\
    \textbf{Multi-Camera}  & Assisted Stereo~\cite{konolige2010projected} & SeemaLab~\cite{eiriksson2015precision}\\
  \bottomrule
  \end{tabular}
\end{table}

Table~\ref{tab:stl_taxonomy} illustrates this taxonomy with examples. The general idea of multiplexing structured light is to compile the multiple patterns into a cohesive set of information that can be used to solve the correspondence problem. Formally let us say that we have a series of patterns $p=\{p_0\dotsc p_{N - 1}\}$ which we sequentially project and record. As such we obtain a series of images $i=\{i_0\dotsc i_{N - 1}\}$, which are to be used in solving the correspondence problem.

There are quite many ways of doing this like e.g. Gray Codes~\cite{posdamer1982surface} or Unstructured Light~\cite{couture2011unstructured}. During this project, phase shifting was primarily used for it's precision and versatility. The following text will cover the base version of phase shifting, which will give an overview of how it operates.

\subsection{Phase Shifting}
\label{sec:bg:ps}
As the name suggests, this category of structured light techniques seeks to encode a unique phase onto the scene. The pattern sequence is defined by the following,
%Unlike binary patterns, the phase shifting techniques uses all available grey levels to encode information. The most basic variety produces a pattern sequence whose values are defined as below,
\begin{align}
  p_n(u, v) = \frac{1}{2} + \frac{1}{2}\cos\left(2\pi\left(\frac{n}{N} + u\right)\right),\label{eq:pspattern_one}
\end{align}
where,
\begin{align*}
  u, v &= \text{normalized projector coordinates},\\
  N    &= \text{sequence steps},\\
  n    &= \text{pattern step}.
\end{align*}
Notice that $v$ is not present in the pattern definition of \eqref{eq:pspattern_one}. The reason for this is that patterns need only be horizontally unique because of the epipolar constraint.
The values in the observed image then is,
\begin{align}
  i_n(x, y) = o(x, y) + a(x, y) \cos\left(\frac{n}{N} + \theta(x, y)\right),\label{eq:psobserve_one}
\end{align}
where,
\begin{align*}
  o(x, y)      &= \text{background illumination},\\
  a(x, y)      &= \text{albedo},\\
  \theta(x, y) &= \text{phase}.
\end{align*}
\begin{figure}
  \centering
  \includegraphics[width = 0.75\textwidth]{graphics/background/ps_one_phase}
  \caption{Illustration of single phase encoding. The signal defined by \eqref{eq:pspattern_one} is projected onto the scene. The observed images are then combined into a single phase-map, which defines $\theta(x, y)$, using \eqref{eq:ps_fourier} and \eqref{eq:ps_one_fourier}}
  \label{fig:ps_one_map}
\end{figure}
So for each pixel, we observed a sinusoidal signal that evolves over the sequence. We refer to this signal as $i(x, y)=\{i_0(x, y), \dotsc, i_{N - 1}(x, y)\}$. The background illumination and albedo of the signal in \eqref{eq:psobserve_one} is not necessarily unique, however $\theta(x, y)$ is unique along epipolar lines. Therefore the goal of phase shifting is to recover $\theta(x, y)$ and use it for the correspondence problem. Indeed all of $\theta(x, y)$ is referred to as a phase map, and the process for creating one is illustrated in Figure~\ref{fig:ps_one_map}. To properly recovering the phase, we apply the \ac{DFT} to this signal:

\begin{align}
  I(x, y) &= \mathcal{F}\left\{i(x, y)\right\},\nonumber\\
          &= \left\{I_0(x, y),\dotsc, I_{N - 1}(x, y)\right\}\label{eq:ps_fourier}
\end{align}
where,
\begin{align*}
  I_n(x, y) &= \text{Fourier components of $i(x, y)$}.
\end{align*}
Ideally, the resulting spectrum should be,
\begin{align}
  I_0(x, y) &= o(x, y)\\
  I_1(x, y) &= Na(x, y)e^{i\theta(x, y)}\\
  I_2(x, y) = \dotsc = I_{N-1}(x, y) &= 0
\end{align}
With this, we obtain the phase by,
\begin{align}
  \theta(x, y) = \arg\left(I_1(x, y)\right)\label{eq:ps_one_fourier}
\end{align}
In practice $I_2(x, y), \dotsc, I_{N-1}(x, y)$ is not exactly zero due to sensor noise and slight variations in background illumination. In fact some of this noise spectrum will likely spill over into $I_1(x, y)$ as dictated by the Nyquist-Shannon sampling theorem. Luckily, this can be mitigated by simply adding more samples to our signal. Thus, phase shifting is quite scalable requiring a minimum of 3 patterns with the option of adding more patterns for more precision.
%There is an upper limit to how accurately the phase can be recovered. Therefore, high-precision phase-shifting projects multiple sequences with different phases that are then combined into a single phase. Again there are many ways of doing this. We will be covering the one primarily used in this thesis.

Typically only real-time applications goes for this base pattern approach. The reason being that there are many external sources of noise (e.g. signal discretization, unstable background illumination and sensor noise) that sets an effective limit to how accurate we can know $\theta(x, y)$. Instead, it is more accurate to observe multiple phases with the above method and combine them. One of the most common ways of doing this, and the technique used in this project, is called phase unwrapping.

\subsubsection{Phase Unwrapping}
The use of phase unwrapping requires that one projects two sinusoidal signals onto the measured scene, one with 1-period and one with $K$-periods.
%In phase unwrapping project two sinusoidal signals onto the measured scene, one with 1-period and one with $K$-periods.
The idea is then to combine the phase of each into a single, accurate phase map. This process is illustrated in Figure~\ref{fig:phase_unwrap}.
The 1-period signal is defined in~\eqref{eq:pspattern_one} which we refer to this pattern sequence as $p = \{p_0, \dotsc, p_{N-1}\}$. Let us then denote the $K$-spatial period signal as $p_K = \{p_{K, 0}, \dotsc, p_{K, N - 1}\}$ with each pattern being defined by,
\begin{align}
  p_{K, n}(u, v) = \frac{1}{2} + \frac{1}{2}\cos\left(2\pi\left(\frac{n}{N} + uK\right)\right),\label{eq:phase_multi}
\end{align}
where,
\begin{align*}
  u, v &= \text{normalized projector coordinates},\\
  N    &= \text{sequence steps},\\
  n    &= \text{pattern step},\\
  K    &= \text{number of spatial periods}.
\end{align*}
In the above, the phase lies in the range $[0, 2K\pi]$. However, the observed phase $\theta_K(x, y)$ is still the same \eqref{eq:psobserve_one} and thus lies in the range $[0, 2\pi]$. Let us refer to the true phase as $\phi_K(x, y)$ which is given by,
\begin{align}
  \phi_K(x, y) = \theta_K(x, y) + 2\pi k(x, y),\label{eq:phase_unwrap}
\end{align}
where,
\begin{align*}
  k(x, y) = \text{some integer giving the period,}
\end{align*}

\begin{figure}
  \centering
  \includegraphics[width = 1.0\textwidth]{graphics/background/ps_multi_phase}
  \caption{Illustration of the encoding process of unwrapping phase shifting. First, two sequences of patterns projected onto the scene are recorded. Then, encoded into two phase-maps, $\theta(x, y)$ and $\theta_K(x, y)$, respectively at the top and at the bottom. Finally they are encoded into a complete phase map via \eqref{eq:phase_unwrap} and\eqref{eq:phase_number}}
  \label{fig:phase_unwrap}
\end{figure}

Phase unwrapping is basically the problem of determining $k(x, y)$ which is the purpose of $p$. For this reason, $p$ is typically referred to as the unwrapper. As $p$ only has one spatial period, it's phase is unambiguous and can thus be used to determine $k(x, y)$. This is done by dividing the range of $\theta(x, y)$ into $K$ parts and then finding the right one. In other words,

\begin{align}
  k(x, y) = \text{round}\left(\frac{K\theta(x, y) - \theta_K(x, y)}{2\pi}\right).\label{eq:phase_number}
\end{align}

By using the results of \eqref{eq:phase_number} in \eqref{eq:phase_unwrap}, we can recover $\phi_K(x, y)$.
%This means we can determine $\phi_K(x, y)$ which can then be used to for very accurate correspondence assignment~\cite{eiriksson2015precision}.

\subsubsection{Advanced Techniques}
The aforementioned technique is very accurate, but also makes critical assumption; the observed signal is only the result of the direct reflection of the projected patterns. This assumption is often broken in two ways in the real world. First, if the background illumination is not constant, it will contaminate the signal spectrum. Second, is if a spatial echo of the signal is measured along with the primary reflection. This is typically the result of either inter-reflections or subsurface scattering. The former refers to light reflected from one part of the scene to another, the latter refers to light being scattered beneath an object's surface. Subsurface scattering can in particular introduce a subtle bias in measured depth, as we have shown in one of the studies included in this thesis~\cite{jensen2017error}.

Various solutions to these problems have been proposed. Authors of~\cite{gupta2012micro} points out that the effect of inter-reflection dependent on the spatial frequency of the projected patterns. They also conclude that lower frequencies are more affected than higher frequencies. Thus, they propose micro phase shifting, where only a narrow band of high-frequent patterns are used. This minimizes the effect of inter-reflections by ensuring that it is approximately the same for all patterns. Modulated phase shifting follows a similar strategy, wrapping the signal in a high frequency carrier wave~\cite{chen2008modulated}.
