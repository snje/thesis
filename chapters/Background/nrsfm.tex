\section{Non-Rigid Structure from Motion}
\label{sec:bg_nrsfm}
\begin{figure}[!b]
  \centering
  \includegraphics[width=0.6\textwidth]{graphics/background/nrsfm_paper_idea}
  \caption{Illustration of \ac{NRSfM}. The general idea is to take a set of 2D observations (typically from images) and produce an estimate of the scene and view geometry. The reconstructions on the right have been created using the algorithm described in~\cite{MultiBody2017}}
  \label{fig:nrsfm_idea}
\end{figure}

\ac{NRSfM} is, as Figure~\ref{fig:nrsfm_idea} illustrates, the science of estimating geometry from a set of 2D observations, that is both view and scene geometry. Unlike regular \ac{SfM}, \ac{NRSfM} makes no rigidity assumption, which makes a much broader and a much harder problem. To see why, let us take a look at the classical \ac{SfM} factorization problem formulated by Tomasi et al.~\cite{Tomasi:Kanade:IJCV92},
\begin{align}
  \mathbf{W} = \mathbf{M}\mathbf{S},\label{eq:sfm}
\end{align}
where,
\begin{align*}
  \mathbf{W} =& \begin{bmatrix}
    u_{11} & u_{12} & \cdots & u_{1P}\\
    v_{11} & v_{12} & \cdots & v_{1P}\\
    u_{21} & u_{22} & \cdots & u_{2P}\\
    v_{21} & v_{22} & \cdots & v_{2P}\\
    \vdots   & \vdots   & \ddots & \vdots\\
    u_{F1} & u_{F2} & \cdots & u_{FP}\\
    v_{F1} & v_{F2} & \cdots & v_{FP}\\
  \end{bmatrix}
  = \begin{bmatrix}
    \mathbf{w}_{11} & \mathbf{w}_{12} & \cdots & \mathbf{w}_{1P}\\
    \mathbf{w}_{21} & \mathbf{w}_{22} & \cdots & \mathbf{w}_{2P}\\
    \vdots   & \vdots   & \ddots & \vdots\\
    \mathbf{w}_{F1} & \mathbf{w}_{F2} & \cdots & \mathbf{w}_{FP}
  \end{bmatrix},\\
  \mathbf{M} =& \begin{bmatrix}
    \mathbf{M}_1 & \mathbf{0}   & \cdots & \mathbf{0}\\
    \mathbf{0}   & \mathbf{M}_2 & \cdots & \mathbf{0}\\
    \vdots       & \vdots       & \ddots & \vdots    \\
    \mathbf{0}   & \mathbf{0}   & \cdots & \mathbf{M}_F
  \end{bmatrix},\\
  \mathbf{S} =& \begin{bmatrix}
    x_{1, 1} & x_{1, 2} & \cdots & x_{1, P}\\
    y_{1, 1} & y_{1, 2} & \cdots & y_{1, P}\\
    z_{1, 1} & z_{1, 2} & \cdots & z_{1, P}\\
    x_{2, 1} & x_{2, 2} & \cdots & x_{2, P}\\
    y_{2, 1} & y_{2, 2} & \cdots & y_{2, P}\\
    z_{2, 1} & z_{2, 2} & \cdots & z_{2, P}\\
    \vdots   & \vdots   & \ddots & \vdots\\
    x_{F, 1} & x_{F, 2} & \cdots & x_{F, P}\\
    y_{F, 1} & y_{F, 2} & \cdots & y_{F, P}\\
    z_{F, 1} & z_{F, 2} & \cdots & z_{F, P}\\
  \end{bmatrix}
  = \begin{bmatrix}
    \mathbf{s}_{11} & \mathbf{s}_{12} & \cdots & \mathbf{s}_{1P}\\
    \mathbf{s}_{21} & \mathbf{s}_{22} & \cdots & \mathbf{s}_{2P}\\
    \vdots       & \vdots       & \ddots & \vdots    \\
    \mathbf{s}_{F1} & \mathbf{s}_{F2} & \cdots & \mathbf{s}_{FP}
  \end{bmatrix},\\
  \mathbf{M}_f =& \text{ 2x3 orthographic projection matrix,}\\
  F =& \text{ number of frames},\\
  P =& \text{ number of points}.
  %\mathbf{M}
\end{align*}
In other words, $\mathbf{W}$ is the observation matrix, $\mathbf{M}$ is the motion matrix and $\mathbf{S}$ is the shape matrix. For \ac{SfM} we have the following constraint,
\begin{align}
  x_{1, p} = x_{2, p} = \cdots = x_{F, p},\\
  y_{1, p} = y_{2, p} = \cdots = y_{F, p},\\
  z_{1, p} = z_{2, p} = \cdots = z_{F, p}.
\end{align}
As such, we know that $\text{rank}(\mathbf{S}) \leq 3$. For \ac{NRSfM} we do not make any such assumptions. This means that the factorization problem in \eqref{eq:sfm} becomes highly ill-posed, as a non-singular corrective transform $\mathbf{G}$ can be applied to arrive at a different valid factorization,
\begin{align}
  \mathbf{W} = \mathbf{M}\mathbf{G}\mathbf{G}^{-1}\mathbf{S}
\end{align}
In order to solve \eqref{eq:sfm} one needs to add the appropriate priors and regularization. While \ac{SfM} is a quite mature field, \ac{NRSfM} still remains a largely unsolved problem. There is not as of yet a clear consensus of the best approach. However, many methods follow the same overall strategy, that is using a low-rank basis.

\subsection{Low-Rank Basis}
This prior was first proposed by Bregler et al.~\cite{bregler2000recovering}. It assumes that the shape in each frame can be modeled as the linear combination of a set of basis shapes of some rank $K$.
As such the general problem of \eqref{eq:sfm} becomes,
\begin{align}
\mathbf{W} = \underbrace{\mathbf{D}(\mathbf{C} \otimes \mathbf{I}_3)}_\mathbf{M}
  \underbrace{
    \begin{bmatrix}
      \mathbf{\hat{S}}_1\\
      \mathbf{\hat{S}}_2\\
      \vdots\\
      \mathbf{\hat{S}}_K
    \end{bmatrix}
  }_\mathbf{S}\label{eq:nrsfm_lowrank}
\end{align}
where,
\begin{align*}
  \mathbf{D} = \begin{bmatrix}
    \mathbf{\hat{R}}_1 & \mathbf{0} & \cdots & \mathbf{0}\\
    \mathbf{0} & \mathbf{\hat{R}}_2 & \cdots & \mathbf{0}\\
    \vdots & \vdots & \ddots & \vdots\\
    \mathbf{0} & \mathbf{0}  & \cdots & \mathbf{\hat{R}}_2\\
  \end{bmatrix},
  \mathbf{C} = \begin{bmatrix}
    c_{1, 1} & c_{1, 2} & \cdots & c_{1, K}\\
    c_{2, 1} & c_{2, 2} & \cdots & c_{2, K}\\
    \vdots & \vdots & \ddots & \vdots\\
    c_{F, 1} & c_{F, 2} & \cdots & c_{F, K}\\
  \end{bmatrix}
\end{align*}
This particular formulation of the problem has become quite popular due to it's simplicity and expressive power, see~\cite{GotardoPAMI2011},~\cite{GotardoICCV2011},~\cite{GotardoCVPR2011}~or~\cite{gotardo:ECCV2012} for examples.
While the formulation in \eqref{eq:nrsfm_lowrank} is much more constrained than the base formulation in \eqref{eq:sfm}, there still quite many unknowns to be determined. Furthermore low-rank shapes only ensures spatial smoothness, but neglects temporal smoothness. For this reason the idea of using a \ac{DCT} basis was introduced. Akhter et al.~\cite{Akhter:etal:2008} first used the \ac{DCT} as a shape basis, however Gotardo et al.~\cite{GotardoCVPR2011} later proposed to use the \ac{DCT} basis to model the weight matrix $\mathbf{C}$ instead,
\begin{align}
  \mathbf{C} = \Omega_d \begin{bmatrix}x_1& x_2 & \cdots & x_K\end{bmatrix} = \Omega_d \mathbf{X}
\end{align}
where,
\begin{align*}
  \Omega_d &= \text{\ac{DCT} basis with $d$ components},\\
  x_k     &= \text{\ac{DCT} coefficient}.\\
\end{align*}
Such that \eqref{eq:nrsfm_lowrank} becomes,
\begin{align}
  \mathbf{W} =  \underbrace{\mathbf{D}(\Omega_d \mathbf{X} \otimes \mathbf{I}_3)}_\mathbf{M}
    \underbrace{
      \begin{bmatrix}
        \mathbf{\hat{S}}_1\\
        \mathbf{\hat{S}}_2\\
        \vdots\\
        \mathbf{\hat{S}}_K
      \end{bmatrix}
    }_\mathbf{\hat{S}}
\end{align}
This ensures temporal smoothness for both shape deformation and for the camera as well. Gotardo et al.~\cite{GotardoCVPR2011} and Ansari et al.~\cite{scalesurface2017} suggest first estimating $\mathbf{D}$ and $\mathbf{X}$, then $\mathbf{\hat{S}}$ can be found by,
\begin{align}
  \mathbf{\hat{S}} = \mathbf{M}^{+}\mathbf{W},
\end{align}
where,
\begin{align*}
  \mathbf{M}^{+} = \text{Moore-Penrose pseudoinverse of $\mathbf{M}$.}
\end{align*}

\subsection{Missing Data}
The base formulation of the \ac{NRSfM} problem in \eqref{eq:sfm} implicitly assumes that the position of all points are known in all frames. However, in the real-world this is rarely the case due to self-occlusion and occlusions from the environment. This adds additional complexity on top of an already difficult problem. Most attempt to deal with this problem as a matrix completion problem, that is estimating the missing entries in $\mathbf{W}$. This can be accomplished using either a \ac{DCT}-basis~\cite{GotardoCVPR2011} or repeated factorizations~\cite{paladini2009factorization}. This described in detail in Section~\ref{sec:rel:missing}.
