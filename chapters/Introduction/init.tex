\chapter{Introduction}
3D vision is the science of estimating 3D geometry from 2D data, typically from images. While it has long been confined to laboratories, recent advances has seen many 3D vision methods move from the lab to application on real-world problems. This is evident by the availability of commercial 3D scanning technology (e.g Kinect, RealSense and GOM) and their many applications (e.g. self-driving cars, robotics and quality control). And it is easy to see why, as 3D vision is a fast and non-destructive method for acquiring rich 3D data. However, as any engineer knows the move from lab to real-world application is not trivial. In this sense, the application of much 3D vision technology is built on an unknown foundation, as there has been few studies on the complications and limitations that appear in the real world.
%\newline
\section{Scope}
This thesis presents a study on the practical applications of 3D vision. We wanted to clearly define current possibilities and challenges. The field of 3D vision is vast therefore the scope of this thesis is limited to studying a handful of problems. However, we do feel that these are representative of the overall state of 3D vision.
\subsection{Evaluation of \acl{NRSfM}}
\label{sec:intro:nrsfm}
\ac{SfM} is the science of estimating 3D geometry from a set of 2D points, typically obtained from an image sequence. It invokes a rigidity prior to constrain the problem. \ac{NRSfM} forgoes this prior, allowing for reconstruction of a deforming scene. While \ac{SfM} is well understood, \ac{NRSfM} is not quite as technologically mature. One reason for this is that \ac{NRSfM} is inherently a more difficult problem than \ac{SfM}. Another is that there has been little effort to evaluate and study the many methods that has been published over the years. Thus it is unknown which algorithms work well and which areas future research should focus on.
\newline

As we see it there are two primary barriers. First, a lack of high-quality, varied datasets with ground truth, which can be used for quantitative study.
Second, a factorial evaluation protocol that can examine the state-of-the-art in \ac{NRSfM} in statistical sound manner. A resolution for both is presented in this thesis. As such, we will make it clear what contemporary \ac{NRSfM} can do and where the main challenges lie.

\subsection{Flexible Automation using 3D Vision}
\label{sec:intro:flex}
Contemporary automation has a rigidity problem, though not one of form, but of setup. Current automation solutions are designed to do one task and one task only. This means that automation is only financially feasible for products that are constantly manufactured. As such, many tasks still has to be performed with 100\% manual labor. Additionally, the robots timing and movement is completely preprogrammed. This means that great care must also be taken in designing the environment around the robot, as unknowns cannot be dealt with. This also means that rigid objects are far easier to deal with than non-rigid.
Therefore, it is of interest to combine 3D vision technology with robotics to create more flexible automation. The idea being that online gathered geometric information can be used to make decisions and adjustments in real-time.
\newline

Flexible automation with 3D vision is studied in this thesis. Not only can we learn which problems are feasible to solve, but we will also uncover the major challenges in integrating vision and robotic path planning.
\newline

The subject will be studied through a use case at Danish Crown, Ringsted. Danish Crown, Ringsted is a large slaughterhouse which make meat products and cutouts. The final stage for a product is packaging, where the cutouts is picked up from an plastic box and placed on a conveyor belt or a cardboard box and then sent to shipping. This process is illustrated in Figure~\ref{fig:intro:meat}. While this process seems simple, it has so far proven infeasible to automate. The reason being that cutout shapes and sizes often changes, which makes offline programmed solutions unworkable. Additionally, products arrives in an unordered pile. As such, We study how 3D vision may be used to overcome both of these challenges.

\begin{figure}[!h]
  \centering
  \begin{minipage}[b]{.4\linewidth}
    \centering
    \includegraphics[width=\textwidth]{graphics/intro/meat}
  \end{minipage}
  \begin{minipage}[b]{.4\linewidth}
    \centering
    \includegraphics[width=\textwidth]{graphics/intro/meatpackage2}
  \end{minipage}
  \caption{The practical use case for this thesis. The deceptively simple process of picking meat from an unorganized pile (left) and placing it on e.g. a conveyor belt.}
  \label{fig:intro:meat}
\end{figure}

\subsection{Error Analysis of Structured Light}
\label{sec:intro:stl}
Structured light scanning is an active 3D scanning technique. It projects one or more patterns onto a scene, captures images and estimates surface geometry based on these. Examples of commercial structured light includes Kinect V1, RealSense and GOM. While structured light has provided a quick and easy way of gathering rich geometric information, it is based on assumptions that is unfortunately often violated in the real-world.

The primary assumption being that the observed signal in the captured images is primarily the results of the direct reflection of the projected pattern. Generally speaking, the real world breaks this assumption in two ways inter-reflections and subsurface scattering. Inter-reflections is when the scene reflects the projected pattern internally. Subsurface scattering describes light entering the scene material and being scattered before being emitted back into the environment.
\newline

The effects of subsurface scattering on structured light scanning is studied in this thesis, as this was previously poorly understood. And it is important to understand as many real world materials (e.g. plastic, biological tissue and cloth) exhibits subsurface scattering.

\subsection{Evaluation of Photorealistic Rendering}
As is evident from the video game industry and cinema, computer graphics has come a long way since it's inception. We are infact capable of creating imagery that closely resemble real world photographies. However, little effort has gone into quantifying the realistism of state-of-the-art rendering. The main reason for this is that creating a dataset with input (geometry, radiometry, camera pose, environment map) and a corresponding ground truth image is extremely difficult.
\newline

Understanding the precision of state-of-the-art computer graphics is quite important for computer vision, as it is increasingly being used to create synthetic training data for deep learning. Indeed, training on synthetic data might not transfer well to the real world, if said data is subtlety biased or flawed.

Therefore, we present the implementation of such a dataset with a corresponding evaluation of state-of-the-art rendering techniques in this thesis.

\subsection{Geometric Metrology using Vision}
\label{sec:intro:metro}
Geometric metrology is the science of geometric measurements and uncertainty estimation. One of it's central concepts is traceablity, which is an unbroken chain of comparisons to a stated reference (e.g. the meter standard) with uncertainty estimates. As such geometric metrology is relevant for 3D vision and vice versa. However, uncertainty estimates and traceability for computer vision is still somewhat of an open question.
\newline

In this thesis we will study establishing traceability and uncertainty for vision using a specific problem. The problem being the automatic measurement of contact surface area using microscopy data.

\section{Objectives}
\label{sec:intro:obj}
So in summary the objectives of this thesis was,
\begin{enumerate}
  \item Create a high-quality, realistic dataset for \ac{NRSfM}.
  \item Evaluate the field of \ac{NRSfM} and find the major challenges.
  \item Create a flexible automation system for bin-picking of meat by integrating 3D vision and robotics.
  \item Study the accuracy of structured light w.r.t. subsurface scattering.
  \item Create a realistic dataset for evaluation of photorealistic rendering, with input (geometry, radiometry, camera pose, environment map) and corresponding ground truth imagery.
  \item Study the uncertainty and traceability of vision by solving the metrological problem of automatic contact surface area estimation.
\end{enumerate}


\section{Thesis Overview}
This thesis is structured as follows:

\begin{description}
  \item[Chapter~\ref{chapter:bg}] \hfill\\
  Here we will go over some of the theory needed to understand this thesis. It will primarily concern camera geometry and \ac{NRSfM}. Structured light scanning has served a substantial role, both in implementing solutions and gathering data. Therefore this chapter also features the basic concepts and theory of structured light scanning.
  \item[Chapter~\ref{chapter:related}] \hfill\\
  In this chapter, we will review the relevant literature. We will primarily be focusing on the main contributions. As such it will be divided into two parts: one concerning state-of-the-art \ac{NRSfM} algorithms and one concerning robotic handling and bin-picking of deformable objects.
  \item[Chapter~\ref{chapter:contributions}] \hfill\\
  In this chapter, we will summarize the primary contribution that has been made during this thesis. It will primary be based on the peer-review publications which are listed in the thesis frontmatter with additional details added as needed. %It will also clarify exactly what contributions can be attributed to this project.
  \item[Chapter~\ref{chapter:conclusion}] \hfill\\
  Here we will summarize the thesis and reflect on it as a whole. We will consider whether the thesis goals has been met.
\end{description}
