\section{\acl{NRSfM}}
\label{sec:related:nrsfm}
As briefly mentioned in section~\ref{sec:bg_nrsfm}, \ac{NRSfM} is still a field undergoing significant development. In the following, we will give a review of related work in \ac{NRSfM}. It should be seen as expanding upon the existing literature review of paper~\ref{paper:nrsfm}. We will first focus on the shape and motion recovering aspects of \ac{NRSfM} and how the idea of low-rank shapes has been implemented in previous work. The base \ac{NRSfM} factorization problem assumes an orthographic camera. However, most real-world image data is obtained via perspective projection. Thus, we will examine how this gap has been bridged in previous work.
As previously mentioned, much of field assumes a complete $\mathbf{W}$ which is rarely the case in real-world data. Thus, a portion of this text will be dedicated to examining how missing data has been dealt with previously.
\newline
%\subsection{Complete Data}

\ac{NRSfM} is an inherently ill-posed problem and thus a solution needs regularization and priors to be physically meaningful. Despite the physical nature of the problem, purely statistical priors are often employed. Methods that use the low-rank basis prior are a part of this category, as are methods which employ spatio-temporal smoothness as well as orthonormality.

Bregler et al.~\cite{bregler2000recovering} was the first to employ a low-rank statistical prior, inspired by Tomasi et al.'s work in \ac{SfM}~\cite{Tomasi:Kanade:IJCV92}. Indeed, they also recover the camera motion and base shapes using the \ac{SVD},
\begin{align}
  \mathbf{W} &=\mathbf{U}\mathbf{\Sigma}\mathbf{V}^T,\\
             &=\mathbf{\hat{M}}\mathbf{\hat{S}}\label{eq:nrsfm_svd}
\end{align}
where,
\begin{align*}
  \mathbf{\hat{M}} &= \mathbf{U}\mathbf{\Sigma}\\
  \mathbf{\hat{S}} &= \mathbf{V}^T
\end{align*}
and simply taking the 3$K$ largest singular vectors and values. While reasonable results were achieved with the above factorization method, it only acts as a constraint on the spatial distribution of the reconstruction. So for a rapidly moving camera, results can quickly deteriorate. Akhter et al.~\cite{Akhter:etal:2008} proposed that a low frequency \ac{DCT} should be used as shape basis instead,
\begin{align}
  \mathbf{W} = \mathbf{D}\mathbf{\Omega_d}\mathbf{S}.
\end{align}
where,
\begin{align*}
  \mathbf{D} &= \text{trace orthographic projection matrix,}\\
  \mathbf{\Omega_d} &= \text{DCT basis with $d$ vectors,}\\
  \mathbf{S} &= \text{shape bases.}\\
\end{align*}
They derive this form by applying a rectification matrix $\mathbf{Q}$ to \eqref{eq:nrsfm_svd},
\begin{align}
  \mathbf{D}\mathbf{\Omega_d} &= \mathbf{\hat{M}}\mathbf{Q}\\
  \mathbf{S}                &= \mathbf{Q}^{-1}\mathbf{\hat{S}}.
\end{align}
$\mathbf{Q}$ is found using the \ac{DCT} basis. This dual representation is referred to as the point trajectory approach. Gotardo et al.~\cite{GotardoCVPR2011} later argue that the trajectory approach could be expanded to the entire shape, viewing the deformation as a smooth point trajectory in $K$ dimensional space,
\begin{align}
  \mathbf{W} =  \underbrace{\mathbf{D}(\Omega_d \mathbf{X} \otimes \mathbf{I}_3)}_\mathbf{M}
    \underbrace{
      \begin{bmatrix}
        \mathbf{\hat{S}}_1\\
        \mathbf{\hat{S}}_2\\
        \vdots\\
        \mathbf{\hat{S}}_K
      \end{bmatrix}
    }_\mathbf{\hat{S}}
\end{align}
The motion matrix $\mathbf{M}$ can then effectively be estimated using their column space fitting algorithm~\cite{GotardoPAMI2011}. The shape basis is then determined by,
\begin{align}
  \mathbf{\hat{S}} = \mathbf{M}^{+}\mathbf{W},
\end{align}
where,
\begin{align*}
  \mathbf{M}^{+} = \text{Moore-Penrose pseudoinverse of $\mathbf{M}$.}
\end{align*}%\fxnote{Also kernel and other variants of this stuff}
This approach was later expanded with the kernel trick, to provide effective means of modeling non-linear deformations like articulated motion~\cite{GotardoICCV2011}.
Torresani et al.~\cite{torresani2008nonrigid} points out that the linear subspace representation of NRSfM in \eqref{eq:nrsfm_lowrank} is inherently quite unstable w.r.t. the size of the subspace. Choose a $K$ that is too large and the problem becomes totally unconstrained, choose a $K$ that is too small and the problem becomes too constrained, unable to accurately model real world motion. Thus, making these methods work would require extensive parameter tuning. They argue that treating the shape estimation as a probabilistic problem is a better approach. Specifically that the shape weights of $\mathbf{C}$ in \eqref{eq:nrsfm_lowrank} should be viewed as a normal distribution,
\begin{align}
  c_{f, k} = \mathcal{N}\left(0, 1\right)
\end{align}
Then $\mathbf{S}$ can be found via an \ac{EM} algorithm. Olsen et al.~\cite{olsen2008implicit} included both temporal and spatial smoothness into their \ac{NRSfM} algorithm by including corresponding penalty terms into an optimization step. Olsen et al.~\cite{bartoli2008coarse} combined this approach with another key assumption; that base shapes $S_k$ are ordered in a coarse to fine manner similar to the components of \ac{PCA}. This means that the first base shapes describes the coarse movements while the later shapes describes finer motion. In their algorithm, shapes are estimated in an iterative manner, allowing for automatic selection of rank $K$ to a certain error threshold. Each shape is determined in an optimization step with spatial and temporal smoothness terms. In a similar spirit, Brandt et al.~\cite{brandt2009uncalibrated} argues that the best way to select the shape base is via statistical independence. They proposed using the independent component analysis to accomplish this.

Kong et al.~\cite{kong2016prior} argues that the linear combination of a number of shapes is too restrictive to express generic deformations. Instead, one should exploit the inherent compressibility of \ac{SfM} to enforce sparsity in the shape basis. As such we can assume a full rank shape basis $\mathbf{S}$ which is sparse in the sense that it only has $K$ non-zero entries for each row. Kong et al.~\cite{kong2016prior} showed that this prior is strong enough to yield decent reconstructions without any additional priors like smoothness.

A low-rank basis implicitly assumes that only one deforming object is present. Kumar et al.~\cite{kumar2017spatio} argues that this is too inflexible to handle real-world scenes, where more than one object is typically present. Instead, they proposed formulating the reconstruction problem as a joint segmentation and reconstruction problem. Indeed, they argue this can be done by exploiting the inherent spatial and temporal clustering of \ac{NRSfM}. This is formulated as a self-expressive property of $\mathbf{S}$. For spatial clustering this is given by,
\begin{align}
  \mathbf{S} &= \mathbf{S}\mathbf{C}_1,
\end{align}
subject to,
\begin{align}
  \text{diag}(\mathbf{C}_1) &= 0,\label{eq:multi_idiot}\\
  \mathbf{1}^T\mathbf{C} &= \mathbf{1}.\label{eq:multi_affine}
\end{align}
Here, some clustering matrix $\mathbf{C}_1$ approximates each column (which is a trajectory) in $\mathbf{S}$ as a linear combination of other columns in $\mathbf{S}$. Constraint \eqref{eq:multi_idiot} ensures that we avoid the obvious solution of $\mathbf{C}_1=\mathbf{I}$ and \eqref{eq:multi_affine} ensures that the combination remains affine. A similar self-expressive constraint can be formulated for trajectories~\cite{kumar2017spatio}. Kumar et al. then shows that an accurate reconstruction can be estimated in solving for the spatio-temporal clustering. While this approach has been designed for multiple bodies, it also works quite will for single bodies as we have shown with our \ac{NRSfM} dataset and evaluation (paper~\ref{paper:nrsfm}).
\newline

All of the above methods deal with an orthographic camera, however most real world image data is best approximated with a perspective camera. A perspective projection $\mathbf{q}$ of a point $\mathbf{s}$ is given by,

\begin{align}
  \lambda \mathbf{q} = \mathbf{P}\mathbf{s}
\end{align}
where,
\begin{align*}
  \mathbf{s} &= \begin{bmatrix}
    x \\ y \\ z \\1
  \end{bmatrix},
  \mathbf{q} = \begin{bmatrix}
    u\\ v \\1
  \end{bmatrix},\\
  \mathbf{P} &= \text{3x4 perspective projection matrix},\\
  \lambda &= \text{projective depth}.
\end{align*}

In other words, perspective projection can be seen as a scaled affine projection. For this reason many \ac{NRSfM} and \ac{SfM} methods account for perspective projection by considering a scaled measurement matrix $\mathbf{\widetilde{W}}$ instead of the original,

\begin{align}
  \mathbf{\widetilde{W}} =
  \begin{bmatrix}
    \lambda_{11}\mathbf{q}_{11} & \lambda_{12}\mathbf{q}_{12} & \cdots & \lambda_{1P}\mathbf{q}_{1P}\\
    \lambda_{21}\mathbf{q}_{21} & \lambda_{22}\mathbf{q}_{22} & \cdots & \lambda_{2P}\mathbf{q}_{2P}\\
    \vdots & \vdots & \ddots & \vdots\\
    \lambda_{F1}\mathbf{q}_{F1} & \lambda_{F2}\mathbf{q}_{F2} & \cdots & \lambda_{FP}\mathbf{q}_{FP}\\
  \end{bmatrix}
\end{align}
where,
\begin{align*}
  \mathbf{q}_{fp} &= \begin{bmatrix}
    \mathbf{w}_{fp}\\
    1
  \end{bmatrix},\\
  \mathbf{w}_{fp} &= \text{ image coordinate as given by \eqref{eq:sfm}},\\
  \lambda_{fp} &= \text{ projective depth}.
%\widetilde{\phi}
\end{align*}

Estimating the projective depths is, of course, not a trivial task, especially for non-rigid scenes. Wang et al. \cite{wang2007structure} proposed a solution where the observation matrix $\mathbf{W}$ is iteratively reweighted. The new weights are calculated from the estimated factorization $\mathbf{M}$'s and $\mathbf{S}$'s deviation from the observed perspective projection.

Hartley and Vidal~\cite{hartley2008perspective} derived a closed-form algrebraic solution for recovering the perspective projection matrices along with the shape basis and shape weights. The algorithm simply requires an initial estimate of a multifocal tensor, though the authors have reported it as being very noise sensitive.

Llado et al.~\cite{llado2010non} proposed a method for segmenting a deforming scene into rigid and non-rigid points using an initial rough projective depth estimation. The camera is then self-calibrated using the rigid subset, which is then used to refine the perspective factorization problem.

Chhatkuli et al.~\cite{chhatkuli2017inextensible} completely forgoes the standard factorization procedure in dealing with perspective. Instead, they resolve the projection depth in second-order cone programming formulation by representing shape as a set of view invariant features. Specifically, they use the assumption of isometry in the form of a maximum depth heuristic.

\subsection{Missing Data}
\label{sec:rel:missing}
In real world observations $\mathbf{W}$ is rarely complete due to occlusions. As such dealing with this missing data is essential for \ac{NRSfM}.
The BALM algorithm~\cite{del2012bilinear} treats shape, motion and missing data filling as a joint iterative optimization problem. Consider that we want to estimate to factorization $\mathbf{W} = \mathbf{M}\mathbf{S}$, but only some values of $\mathbf{W}$ are known. Let this set of known values be the set $O = \{(i, j) : \mathbf{W}_{i, j} \text{ is known}\}$. Now instead of optimizing on $\mathbf{W}$, we instead optimize on a function $Y(\mathbf{Z})$ which fills $\mathbf{W}$'s unknown entries with estimates $\mathbf{Z}$.
In other words, $Y(\mathbf{Z})$ is defines as,

\begin{align}
  Y(\mathbf{W}, \mathbf{Z})_{ij} =
  \begin{cases}
    \mathbf{w}_{ij},& \text{if} (i,j) \in O\\
    \mathbf{z}_{ij},& \text{otherwise}
  \end{cases}\label{eq:md_fill}
\end{align}

Then we want to optimize on the following loss function,

\begin{align}
  L(\mathbf{W}, \mathbf{Z}, \mathbf{S}, \mathbf{M}; \mathbf{\lambda}) = ||Y(\mathbf{W}, \mathbf{Z}) - \mathbf{M}\mathbf{S}||^2 + \Lambda(\mathbf{M}, \mathbf{\lambda}),\label{eq:md_balm}
\end{align}
where,
\begin{align*}
  \Lambda &= \text{Lagrangian constraint function,}\\
  \mathbf{\lambda} &= \text{Lagrange Multipliers.}\\
\end{align*}
Note that \eqref{eq:md_balm} is a simplified version of the one found in~\cite{del2012bilinear}. Specifically, it excludes a manifold projection penalty term as it is not important for understanding how the algorithm deals with missing data. $\mathbf{M}$, $\mathbf{S}$ and $\mathbf{Z}$ are then determined by iteratively solving \eqref{eq:balm1} and \eqref{eq:balm2},

\begin{align}
  (\mathbf{S}^{k + 1}, \mathbf{M}^{k + 1}) &= \argmin_{\mathbf{S}, \mathbf{M}}
    L(\mathbf{W}, \mathbf{Z}^k, \mathbf{S}, \mathbf{M}; \mathbf{\lambda}),\label{eq:balm1}\\
  \mathbf{Z}^{k + 1} &= \argmin_{\mathbf{Z}}
    L(\mathbf{W}, \mathbf{Z}, \mathbf{S}^{k + 1}, \mathbf{M}^{k + 1}; \mathbf{\lambda}),\label{eq:balm2}
\end{align}
where $\mathbf{S}^k, \mathbf{M}^k, \mathbf{Z}^k$ denotes the results of iteration $k$. In other words $\mathbf{Z}$ is estimated, based on the best estimates of $\mathbf{S}$ and $\mathbf{M}$ and vice versa.

Paladini et al.~\cite{paladini2009factorization} follows a similar iterative missing data estimation approach. Indeed, theirs is quite generic, requiring only an initial estimate of a filled $\mathbf{W}$ via $\mathbf{Z}$. The complete procedure is described in Algorithm~\ref{algo:metric_fill}.

\begin{algorithm}
  \For{$k\in K$} {
    Fill missing entries: $\mathbf{Y}^{[k]} = Y(\mathbf{W}, \mathbf{Z}^{[k]})$\\
    Estimate centroid: $\mathbf{t}^{[k]} = \begin{bmatrix}
      E\left[\mathbf{y}^{[k]}_{1*}\right] & E\left[\mathbf{y}^{[k]}_{2*}\right] & \cdots & E\left[\mathbf{y}^{[k]}_{F*}\right]
    \end{bmatrix}^\mathrm{T}$\\
    Remove centroid: $\mathbf{\hat{Y}}^{[k]} = \mathbf{Y}^{[k]} - \begin{bmatrix} \mathbf{t}^{[k]} & \mathbf{t}^{[k]} & \cdots & \mathbf{t}^{[k]}  \end{bmatrix}$\\
    Solve \ac{NRSfM} factorization: $\mathbf{\hat{Y}}^{[k]} = \mathbf{M}^{[k]}\mathbf{S}^{[k]}$\\
    Add centroid: $\mathbf{Z}^{[k + 1]} = \mathbf{M}^{[k]}\mathbf{S}^{[k]} + \begin{bmatrix} \mathbf{t}^{[k]} & \mathbf{t}^{[k]} & \cdots & \mathbf{t}^{[k]}  \end{bmatrix}$
  }
  \caption{Iterative factorization and missing data estimation algorithm~\cite{paladini2009factorization}. Note that $E[*]$ denotes the expectation (or mean) operator. $Y(\mathbf{W}, \mathbf{Z}^{[k]})$ is as defined in \eqref{eq:md_fill}. Runs for $K$ iterations or until convergence.}
  \label{algo:metric_fill}
\end{algorithm}

Another strategy for dealing with missing data is to fill the missing entries in $\mathbf{W}$ before applying a \ac{NRSfM} algorithm, as was done in~\cite{GotardoCVPR2011}~and~\cite{GotardoPAMI2011}. Indeed, they assume that, similar to their camera trajectory, that each projected point trajectory can be expressed in terms of a low frequency \ac{DCT} basis. With this assumption they recover an initial $\mathbf{M}$ and $\mathbf{S}$ from the known entries, which is used to fill the missing entries in $\mathbf{W}$.

Chhatkuli et al.~\cite{chhatkuli2017inextensible} poses the \ac{NRSfM} reconstruction problem as an optimization problem. As such they simply do not include the missing terms in their optimization algorithm.
