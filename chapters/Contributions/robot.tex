\section{Flexible Robotics for Bin-Picking of Non-Rigid Objects}
\label{sec:contribution:robot}
This section provides an overview our work in solving the bin-picking problem at Danish Crown as described in Section~\ref{sec:intro:flex}. Details can be found in paper~\ref{paper:dc_robot}. Our system for solving this task can be seen in Figure~\ref{fig:contribution:dcrobotic}. It consists of four core components:
%Our applied work with deformable objects has primarily been done centered on a use-case from the slaughterhouses of Danish Crown. On the surface it is quite simple; once the meat has been cut and processed it goes to the packaging facility for shipment. The meat arrives in plastic boxes meant for internal use and must be picked up and placed on a conveyor belt or in another box (e.g. cardboard). Currently this process relies completely on manual labor and we looked into solving it with robotics.%\fxnote{figure that shows a guy doing hte picking}

\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\textwidth]{graphics/contribution/robotics.png}
  \caption{The automated solution for bin-picking that has been developed during this thesis. A structured light scanner is used to recover surface geometry, which is then used to guide the robotic arm with suction cup gripper.}
  \label{fig:contribution:dcrobotic}
\end{figure}



\begin{figure}[t]
  \centering
  \includegraphics[width=0.8\textwidth]{graphics/contribution/segmentation}
  \caption{Example of the segmentation algorithm in process. Each region/instance is given by a separate color. Last stage is selecting the meat piece to be picked, which is a decision weighted on depth and size.}
  \label{fig:contribution:seg}
  \vspace{0.2cm}
  \includegraphics[width=\textwidth]{graphics/contribution/robotlift}
  \caption{Example of the robot cell doing bin-picking. First frame shows a pattern from the structured light scanner. Second frame shows the robot placing the gripper for lift. Third frame is the lift in action.}
  \label{fig:contribution:robolift}
\end{figure}

\begin{description}
    \item[\textbf{Vision}] \hfill \\ There are two subparts to this system. First, a structured light scanner which recovers the surface geometry of the box content. Second, a fast segmentation algorithm that separates each meat piece instance. Developed during this thesis.
    \item[\textbf{Robot Arm}] \hfill \\ Standard issue 6-\ac{DoF} robotic arm.
    \item[\textbf{Suction Cup Gripper}] \hfill \\ Gripper intended to provide flexible gripping without damage the meat. Suction cup positions can be adjusted to account for different cutout sizes and shapes. Developed by Jørgensen et al.~\cite{GripperPaper}.
    \item[\textbf{Simulation Framework}] \hfill \\ Runs simulation of meat handling and optimizes the robots movement in terms of fast and proper placement. Also yields the optimal suction cup placement for the gripper. Developed by Troels Bo Jørgensen of the Southern University of Denmark.
\end{description}

During online operations, the vision system detects the target piece which is then fed to the path planning algorithm. This algorithm has been parameter tuned using the simulation framework, and generates and optimal path for picking and placing. The suction cup gripper is then placed on the meat piece, which is then lifted and placed. This process is then repeated until the box has been emptied. The system was implemented using \ac{ROS}~\cite{ROS2009}.

The vision component of this system was developed during this thesis. It uses the structured light method described in Section~\ref{sec:bg:ps} to acquire 3D information. This information is then fed to an instance segmentation algorithm that we have developed. It is based on the idea of region growth segmentation which is implemented in \ac{PCL}~\cite{Rusu_ICRA2011_PCL, pcl_region}. Briefly described, it grows a region from a seed of low curvature and terminates at high curvature which is typically at the edge of an object instance. The data from phase shifting structured light arrives in a 2D grid, which we exploit to greatly reduce the region growth runtime. Specifically, the algorithm goes through many neighborhood searching steps, which was originally done in 3D Cartesian coordinate system. We instead search for neighbors in the 2D grid which is why our implementation can segment a 675x540 point cloud in 100-150ms on a laptop~\footnote{Specifically on an Intel Core i7-4610M}. The segmentation algorithm is illustrated in Figure~\ref{fig:contribution:seg}.

\subsection{Discussion}
We have demonstrated that it is possible to implement a flexible system for bin-picking of non-rigid objects. We successfully tested it on several cutouts of meat. That said our experience also shows that it is quite challenging to control automation with 3D vision in a stable manner. One major problem is the aggregation of error from the various components. Individually, the error from simulation, calibration, scanning, segmentation and path planning might seem insignificant, but they can quickly add up to cause major problems. As such one must expect partially erroneous data when designing the systems module.

Another major challenge is the sheer number of unique situations the system can encounter in a real world industrial hall. This is especially true when working with unorganized bins of non-rigid objects. It is very easy to be blinded by good performance on a dataset, and then be unpleasantly surprised when testing in the real environment. It is therefore incredible important to test early and test often in the actual operational environment when developing applied computer vision solutions. A dataset is fine as a performance indicator, but it is important to realize that it only encompasses a subset of the problem domain.

Another good argument for early testing is that some error sources can only be uncovered this way. For example, we found that specular reflections from the ceiling lighting would interfere with our structured light scanner. Varying sunlight due to changing cloud cover would also cause disruptions.

\section{Error Analysis of Structured Light Scanning of Biological Material}
Biological material exhibits heavy subsurface scattering properties, e.g. only 5-7\% of the light transmitted by human skin is the results of a direct reflection. To ensure that the structured light method used in the robotic solution produces accurate data, we engaged in a study to ascertain the effects of subsurface scattering on the precision of structured light.

As covered in Section~\ref{sec:intro:stl}, we wanted to examine how subsurface scattering influence structured light scanning. This was also motivated by our development of the flexible robotics cell at Danish Crown.
%This work is covered in paper~\ref{paper:stl_error}. In this work we primarily studied scanning of biological materials as they typically scatters incoming light beneath the surface. For example, only 5-7\% of light reflected by human skin is the result of a direct reflection, the rest is from subsurface scattering~\cite{krishnaswamy2004biophysically}.

Our work is covered in paper~\ref{paper:stl_error}. The approach was simple, scan the surface of some meat. Then coat said meat in a thin layer of diffuse material, which is our case was chalk. Scanning the coated meat then yields the true surface, which we used as a reference. Then we simply applied a standard linear model, based on surface normals and view geometry, to model the error behavior. We discovered that subsurface scattering largely causes a positive bias to structured light scanning, which means the scanned surface seems to be further away than it actually is. This bias is dependent on the specific material and structured light method used and can be up to 1mm. We also found that the error is largely systematic and can be corrected for using the aforementioned linear model. Before correction, methods like micro phase shifting~\cite{gupta2012micro} and modulated phase shifting~\cite{chen2008modulated} has a lower scanning error than standard phase shifting. However after correction with the fitted model, the three methods actually have approximately the same average scanning error.

\section{Other Contributions}
Our work in creating a dataset for evaluation of photorealistic rendering is documented in paper~\ref{paper:scene_reassembly}. We created input data for scenes with glass objects as well as diffuse geometry. The dataset includes fully defined camera pose, BRDFs, scene geometry and environment lightning maps as well as a ground truth image for each camera pose. This was accomplished using the robotics setup shown in Figure.~\ref{fig:stl} as well as CT scanning.
\newline

Our efforts in applying computer vision to the geometric metrology problem described in Section~\ref{sec:intro:metro} were also successful. The work is documented in poster~\ref{poster:contact_area}. %It is interesting to note that while the vision system did perform consistently, we discovered a significant overestimation in this area. However, it turned out that this was a systematic error, meaning it could be corrected for. We only discovered this by establishing traceability for the system. This demonstrated to us that proper traceability will be quite important for 3D vision in future research as biases and errors can be discovered.
