\section{Evaluation of \acl{NRSfM}}
\label{sec:contribution:nrsfm}
As shown in Section~\ref{sec:related:nrsfm}, many \ac{NRSfM} methods have been proposed. So many, that it is unclear what the field can do and where the challenges are. Thus, in the following, we will go over this thesis's evaluation of \ac{NRSfM}. This is divided into two parts; the creation of a proper dataset and a factorial analysis. Our work also provides a basis for future evaluation and studies.
%3D reconstruction from monocular image sequence would be an incredibly powerful tool for robotics and real-world automation. Especially when dealing non-rigid objects. As shown in Section~\ref{sec:related:nrsfm} there is quite the wealth of methods for dealing with the \ac{NRSfM} problem. In fact the field is in dire need of coalescence as it is by no means clear what works well and what doesn't. Thus we choose to advance the field by performing a thorough comparative evaluation and laying the ground work for future progression. Our contribution is two-fold. First we created a high-quality \ac{NRSfM} dataset with ground truth, addressing needs such as realistic missing data, varied deformations and controlled camera motion. Second we have constructed an evaluation pipeline with performs evaluation in a statistical sound fashion. We introduce the usage of \ac{ANOVA} into the field of \ac{NRSfM}. Thus we not only ensure the statistical robustness of our evaluation. Additional it allows us to explore interesting factors in \ac{NRSfM} such as the camera's influence on reconstruction quality. We will go into details about these contributions in the following text.

\subsection{Dataset}
\label{contribution:dataset}
In our opinion, the lack of coalescence in \ac{NRSfM} is largely due to a lack of interesting, realistic data with ground truth. Therefore, we set out to create one. Formally a \ac{NRSfM} dataset consists of three correlated types of data:
\begin{description}
  \item[Observations] \hfill\\
  A set of 2D points which is to be given to a \ac{NRSfM} algorithm as input, denoted as matrix $\mathbf{W}$.
  \item[Missing Data] \hfill\\
  Indicative data which shows which observation points are visible. Given to a \ac{NRSfM} algorithm as input along with the observations.
  \item[Ground Truth] \hfill\\
  The 3D deformation corresponding to the 2D observations, denoted as matrix $\mathbf{S}$.
\end{description}

As described in paper~\ref{paper:nrsfm}, our \ac{NRSfM} dataset contains several innovations. First we used stop-motion mechatronics to approximate real deformations. This allowed us to record the ground truth with very precise structured light scanning. To be specific, we employed the robot-mounted structured light scanner, shown in Figure~\ref{fig:stl}, used in previous work at DTU~\cite{stets2017scene,aanaes2015our,NRSFMpaper,aanaes2016large}.
It also allowed for us to include varied deformations. To be specific, our dataset includes five deformation types; articulated motion (Figure~\ref{fig:mecha:art}), bending (Figure~\ref{fig:mecha:bend}), deflation (Figure~\ref{fig:mecha:deflate}), stretching (Figure~\ref{fig:mecha:stretch}) and tearing (Figure~\ref{fig:mecha:tear}).

Like other data used in evaluation \ac{NRSfM} (like MOCAP) we create observations by projecting the ground truth using a synthetic camera. However, unlike previous work we create observations according to a factorial design. Specifically, we have six different camera paths and two camera models. An observation matrix is then created for each factorial combination. Our evaluation shows that this factorial design is quite important as the camera has a significant influence on the reconstruction error, regardless of the algorithm used.

Unlike previous work, we create missing data based on self-occlusions. The aforementioned structured light scans not only records the ground truth, but also gives us complete, dense surface reconstructions. As such, creating missing data can be done by raycasting from each point into the camera, while occlusion testing against the recorded surface. As we will later see, this kind of realistic missing data is very different from the randomly removed missing data used in previous work.

The ground truth and observation points were sampled using standard optical flow procedure. Therefore we obtain a more naturally distributed set points compared to marker-based motion capture.

\begin{figure}[!t]
  \centering
    \begin{minipage}[b]{.19\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/art}
      \subcaption{Articulated}\label{fig:mecha:art}
    \end{minipage}
    \begin{minipage}[b]{.19\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/paper}
      \subcaption{Bending}\label{fig:mecha:bend}
    \end{minipage}
    \begin{minipage}[b]{.19\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/balloon}
      \subcaption{Deflation}\label{fig:mecha:deflate}
    \end{minipage}
    \begin{minipage}[b]{.19\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/stretch}
      \subcaption{Stretching}\label{fig:mecha:stretch}
    \end{minipage}
    \begin{minipage}[b]{.19\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/tear}
      \subcaption{Tearing}\label{fig:mecha:tear}
    \end{minipage}
    \caption{Stop-motion mechatronics used for \ac{NRSfM} dataset creation.}
    \label{fig:animatronics}
\end{figure}

\begin{figure}[!b]
  \centering
    \begin{minipage}[b]{.45\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/stl_head}
      \subcaption{Scanner}\label{fig:stl:head}
    \end{minipage}\hspace{0.5cm}
    \begin{minipage}[b]{.45\linewidth}
      \centering
      \includegraphics[width=\textwidth]{graphics/contribution/stl_box}
      \subcaption{Environment}\label{fig:stl:box}
    \end{minipage}
    \caption{The mobile structured light scanner that was used for recording our \ac{NRSfM} dataset.}
    \label{fig:stl}
  \end{figure}


\subsection{Evaluation Pipeline}
\label{sec:contribution:evaluation}

%Our contributions to the evaluation pipeline is two-fold. First we defined a robust error metric with a well-defined alignment step. Second we introduced a factorial analysis which provided a nuanced insight into what affects the reconstruction error of \ac{NRSfM}. Apart from surface level details, like algorithm performance ranking, this also allowed us to gleam overall trends in the state of the art.



\begin{figure}[!t]
  \centering
  \includegraphics[width=0.7\textwidth]{graphics/contribution/algo_box}
  \caption{Boxplot of the error distribution of the five \ac{NRSfM} algorithms with the lowest mean error. The distribution has been adjusted for factors like camera motion and deformation type. Notice how closely the means of each distribution resemble each other.}
  \label{fig:contribution:algobox}
\end{figure}


Our evaluation is based on a factorial analysis using \ac{ANOVA}. It is described in paper~\ref{paper:nrsfm}. While \ac{ANOVA} is an well established statistical tool for comparing distributions, it has, to our knowledge, never been applied for \ac{NRSfM} (or most comparisons in computer vision for that matter). The previous standard has been to simply aggregate some collected error metric for various categories and analyzing the difference without regard for whether the difference is statistical significant. Not only can the \ac{ANOVA} help to avoid this kind of type I error, it can also separate the influence of various factors on reconstruction quality.

To be precise, our \ac{ANOVA} model included the influence of five factors on the reconstruction error. These were as follows;
\begin{description}
    \item[\textbf{Algorithm} $a_i$] \hfill \\ Which algorithm was used.
    \item[\textbf{Camera Model} $m_j$] \hfill \\ Was the camera model perspective or orthographic.
    \item[\textbf{Animatronics} $s_k$] \hfill \\ Which animatronics sequence was reconstructed.
    \item[\textbf{Camera Path} $p_l$] \hfill \\ How did the camera move.
    \item[\textbf{Missing Data} $d_n$] \hfill \\ Whether occlusion based missing data was used.
\end{description}

Our model allowed us to learn much about the state-of-the-art in \ac{NRSfM}. Our first analysis without missing data revealed that there is a statistical significant difference between the average reconstruction error for all 16 \ac{NRSfM} methods included. This is not particularly surprising, so we redid the same factorial analysis with the 5 algorithms that had the lowest mean error. These were MultiBody \cite{MultiBody2017}, KSTA \cite{GotardoICCV2011}, RIKS \cite{gotardo:ECCV2012}, CSF2 \cite{GotardoCVPR2011} and MetricProj \cite{DelBue:Agapito:IJCV2011}. Interestingly, the analysis revealed no significant difference.
Indeed, visualization the error distribution of each after correcting for other factors supports this hypothesis\footnote{Using the residuals of an \ac{ANOVA} model without algorithm terms.}. This is shown in Figure~\ref{fig:contribution:algobox}, as can be seen the error distributions are very similar. Introducing our occlusion-based missing data into the model changes this conclusion however. Almost all methods see a large increase in reconstruction error when subjected to our occlusion-based missing data. The only algorithm that is relatively stable is MetricProj~ \cite{DelBue:Agapito:IJCV2011}. Curiously the authors of the method designed their method around the spatio-temporal structure of missing data, whereas other merely focus on the ratio.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.6\textwidth]{graphics/contribution/ort_per_scatter}
  \caption{Repeated reconstructions with varied camera model. Each point is the reconstruction a factorial combination of algorithm, camera path and deformation under orthographic and perspective camera model. As can be seen there is a rough linear trend. This was intended to be included in paper~\ref{paper:nrsfm}, but cut due to page limit.}
  \label{fig:ort_per_scatter}
\end{figure}
%Talk about algorithm ranking and the dangers of overall mean.

The camera model has long been an open question in \ac{NRSfM}. Specifically, employing a perspective camera model has proven to be challenging. Indeed, we observed this in paper~\ref{paper:nrsfm} as only 2 out the 16 included methods uses a perspective camera model. However, our study indicates that the employed camera model is actually not that important. While the camera model does have a statistically significant influence on the reconstruction error, it is small compared to the influence of the deformation type and camera path. Indeed, the few perspective methods we tested did not significantly outperform methods that employ an affine camera on perspectively projected observations. A look at the data, shown in Figure~\ref{fig:ort_per_scatter}, illustrates an interesting trend. The relationship between a reconstruction done under affine and perspective camera can be approximately modeled as line. This line has an approximate unit slope and a positive intercept. Thus, reconstructing perspectively projected observations adds a small constant increase in error, compared to reconstructions using orthogonally projected observations. Though it should be noted that the distance changes in the used camera paths are not particularly extreme, thus keeping perspective artifacts to a minimum (distance changes with a factor 1.6 on average).
\newline

We concluded that deformation type has a significant influence on reconstruction error. Particularly articulated motion and stretching (shown in Figure~\ref{fig:mecha:art} and \ref{fig:mecha:stretch}) results in a large reconstruction error, no matter which algorithm is used. Indeed, we also showed that the camera path has a significant impact on reconstruction, independent of algorithm used.

\subsection{Discussion}
Our evaluation results has several implications as to the future development of \ac{NRSfM}. Most of the state-of-the-art methods handle occlusion-based missing data poorly, thus this area needs attention. It is unclear whether it is the algorithms themselves or their matrix completion that needs improvement. For example, a DCT basis is often used for track completion before doing \ac{NRSfM}. Perhaps the successful completion method of MetricProj~ \cite{DelBue:Agapito:IJCV2011} could be used with other algorithms.

Our studies shows the camera model to be have a small influence on the reconstruction error. Thus, we do not see at being a priority to employ a perspective projection model, especially considering the needed effort.

Articulated motion is an issue which should be dealt with in future work. The non-linear motion of the densely sampled joints poses a challenge regardless of employed prior.

Our analysis also demonstrates the need for controlling the camera. Especially, the camera motion has a significant impact on reconstruction error. Thus employing a taxonomy similar to our work would be benefitial. The influence of the camera path also indicates that future \ac{NRSfM} research should investigate how to deal with this variance.
